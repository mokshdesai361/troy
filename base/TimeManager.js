/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

module.exports = class TimeManager {
    constructor(client) {
        Object.defineProperty(this, 'client', { value: client })

        this.timeouts = new Map()
    }

    async fetchAll() {
        const timers = await this.client.redis.hgetall('timer')
        for (let data of Object.values(timers)) {
            data = JSON.parse(data)
            await this.setTimer(data.channelID, new Date(data.time) - new Date(), data.userID, data.title, false)
        }
        return this
    }

    async setTimer(channelID, time, userID, title, updateRedis = true) {
        const data = { time: new Date(Date.now() + time).toISOString(), channelID, userID, title }
        const timeout = setTimeout(async () => {
            try {
                const channel = await this.client.channels.fetch(channelID)
                await channel.send(`🕰️ <@${userID}>, you wanted me to remind you of: **"${title}"**.`)
            }
            finally {
                await this.client.redis.hdel('timer', `${channelID}-${userID}`)
            }
        }, time)
        if (updateRedis) await this.client.redis.hset('timer', { [`${channelID}-${userID}`]: JSON.stringify(data) })
        this.timeouts.set(`${channelID}-${userID}`, timeout)
        return timeout
    }

    deleteTimer(channelID, userID) {
        clearTimeout(this.timeouts.get(`${channelID}-${userID}`))
        this.timeouts.delete(`${channelID}-${userID}`)
        return this.client.redis.hdel('timer', `${channelID}-${userID}`)
    }

    exists(channelID, userID) {
        return this.client.redis.hexists('timer', `${channelID}-${userID}`)
    }
}