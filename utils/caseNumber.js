/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

async function caseNumber(client, modLog) {
    const messages = await modLog.messages.fetch({ limit: 10 })
    const log = messages.filter(filterMessage => filterMessage.author.id === client.user.id &&
        filterMessage.embeds[0] &&
        filterMessage.embeds[0].type === 'rich' &&
        filterMessage.embeds[0].footer &&
        filterMessage.embeds[0].footer.text.startsWith('Case'),
    ).first()
    if (!log) return 1
    const thisCase = /Case\s(\d+)/.exec(log.embeds[0].footer.text)
    return thisCase ? parseInt(thisCase[1]) + 1 : 1
}

module.exports = { caseNumber }