/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const { ArgumentType } = require('discord.js-commando')
const fileTypeRe = /\.(jpe?g|png|gif|jfif|bmp)$/i
const request = require('node-superfetch')

module.exports = class ImageArgumentType extends ArgumentType {
    constructor(client) {
        super(client, 'image')
    }

    async validate(value, msg, arg) {
        const attachment = msg.attachments.first()
        if (attachment) {
            if (attachment.size > 8e+6) return 'Please provide an image under 8 MB.'
            if (!fileTypeRe.test(attachment.name)) return 'Please only send PNG, JPG, BMP, or GIF format images.'
            return true
        }
        if (fileTypeRe.test(value.toLowerCase())) {
            try {
                await request.get(value)
                return true
            }
            catch {
                return false
            }
        }
        return this.client.registry.types.get('user').validate(value, msg, arg)
    }

    async parse(value, msg, arg) {
        const attachment = msg.attachments.first()
        if (attachment) return attachment.url
        if (fileTypeRe.test(value.toLowerCase())) return value
        const user = await this.client.registry.types.get('user').parse(value, msg, arg)
        return user.displayAvatarURL({ format: 'png', size: 512 })
    }

    isEmpty(value, msg, arg) {
        if (msg.attachments.size) return false
        return this.client.registry.types.get('user').isEmpty(value, msg, arg)
    }
}
