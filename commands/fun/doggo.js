/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const fetch = require('node-fetch')

module.exports = class DoggoCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'doggo',
            description: 'Finds some cute doggo images.',
            group: 'fun',
            memberName: 'doggo',
            examples: ['doggo'],
            aliases: ['dog', 'doggy', 'goodboi'],
            clientPermissions: ['EMBED_LINKS'],
        })
    }

    async run(message) {

        const doggo = await fetch('https://dog.ceo/api/breeds/image/random')
            .then(response => response.json())

        if (doggo.status !== 'success') return message.channel.send('Can\'t find any good boi photo. Report bug with /bug')

        const doggoEmbed = new MessageEmbed()
            .setColor('RANDOM')
            .setTitle('Woof Woof 🐶')
            .setDescription('A cute doggo image for you.')
            .setImage(doggo.message)
            .setFooter(`Powered by ${message.client.user.username}`, message.client.user.avatarURL())
            .setTimestamp()
        return message.channel.send(doggoEmbed)
    }

}
