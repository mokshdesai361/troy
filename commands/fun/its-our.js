/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')

module.exports = class ItsOurCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'our',
            description: 'Firmly states that it is ours in this soviet soil.',
            throttling: { usages: 2, duration: 10 },
            group: 'fun',
            memberName: 'our',
            aliases: ['its-our'],
            examples: ['our'],
        })
    }

    run(message) {
        const imageUrl = 'https://i.kym-cdn.com/entries/icons/original/000/034/467/Communist_Bugs_Bunny_Banner.jpg'
        const itsOurEmbed = new MessageEmbed()
            .setColor('DARK_RED')
            .setTitle('It\'s our')
            .setImage(imageUrl)
            .setFooter(`Powered by communist ${message.client.user.username}`, message.client.user.avatarURL())
            .setTimestamp()
        return message.channel.send(itsOurEmbed)
    }
}