/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')

module.exports = class Reboot extends Command {
    constructor(client) {
        super(client, {
            name: 'reboot',
            description: 'Restarts the bot.',
            aliases: ['restart'],
            group: 'mod',
            ownerOnly: true,
            memberName: 'reboot',
            examples: ['reboot'],
        })
    }

    hasPermission(message, ownerOverride) {
        return super.hasPermission(message, ownerOverride)
    }

    run(message) {
        console.log('Commencing restart process!')
        message.react('👋').catch(console.error)
        return message.author.send('Commencing restart process!\n**Beware, If I am not running via pm2, You need to do start the process manually.**')
            .then(() => {
                process.exit()
            }).catch(reason => {
                console.error(reason.messages)
            })
    }
}