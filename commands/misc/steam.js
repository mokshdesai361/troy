/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const request = require('node-superfetch')
const { formatNumber } = require('../../utils/Helper')

module.exports = class SteamCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'steam',
            group: 'misc',
            memberName: 'steam',
            description: 'Searches Steam for your query.',
            clientPermissions: ['EMBED_LINKS'],
            credit: [
                {
                    name: 'Valve',
                    url: 'https://store.steampowered.com/',
                    reason: 'API',
                },
            ],
            args: [
                {
                    key: 'query',
                    prompt: 'What game would you like to search?',
                    type: 'string',
                },
            ],
        })
    }

    async run(message, { query }) {
        try {
            const id = await this.search(query)
            if (!id) return message.say('Could not find any results.')
            const data = await this.fetchGame(id)
            const current = data.price_overview ? `$${data.price_overview.final / 100}` : 'Free'
            const original = data.price_overview ? `$${data.price_overview.initial / 100}` : 'Free'
            const price = current === original ? current : `~~${original}~~ ${current}`
            const platforms = []
            if (data.platforms) {
                if (data.platforms.windows) platforms.push('Windows')
                if (data.platforms.mac) platforms.push('Mac')
                if (data.platforms.linux) platforms.push('Linux')
            }
            const embed = new MessageEmbed()
                .setColor('RANDOM')
                .setAuthor('Steam', 'https://i.imgur.com/xxr2UBZ.png', 'http://store.steampowered.com/')
                .setTitle(data.name)
                .setURL(`http://store.steampowered.com/app/${data.steam_appid}`)
                .setThumbnail(data.header_image)
                .addField('❯ Price', price, true)
                .addField('❯ Meta Critic score', data.metacritic ? data.metacritic.score : '???', true)
                .addField('❯ Recommendations', data.recommendations ? formatNumber(data.recommendations.total) : '???', true)
                .addField('❯ Platforms', platforms.join(', ') || 'None', true)
                .addField('❯ Release Date', data.release_date ? data.release_date.date : '???', true)
                .addField('❯ DLC Count', data.dlc ? formatNumber(data.dlc.length) : 0, true)
                .addField('❯ Developers', data.developers ? data.developers.join(', ') || '???' : '???')
                .addField('❯ Publishers', data.publishers ? data.publishers.join(', ') || '???' : '???')
            return message.embed(embed)
        }
        catch (err) {
            return message.reply(`Oh crap, an error occurred: \`${err.message}\`. Try again later!`)
        }
    }

    async search(query) {
        const { body } = await request
            .get('https://store.steampowered.com/api/storesearch')
            .query({
                cc: 'us',
                l: 'en',
                term: query,
            })
        if (!body.items.length) return null
        return body.items[0].id
    }

    async fetchGame(id) {
        const { body } = await request
            .get('https://store.steampowered.com/api/appdetails')
            .query({ appids: id })
        return body[id.toString()].data
    }
}
