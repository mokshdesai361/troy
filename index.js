/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Client = require('./base/Client')
const path = require('path')
const chalk = require('chalk')
const Statcord = require('statcord.js')
const fetch = require('node-fetch')
const schedule = require('node-schedule')

const config = require('./config.json')

const { formatNumber } = require('./utils/Helper')


const client = new Client({
    commandPrefix: `${config.prefix}`,
    owner: `${config.ownerId}`,
    disableEveryone: true,
})

let statcord = null

let ayodhyaWeather = null

if (!config.debug) {
    statcord = new Statcord.Client({
        client,
        key: config.statCordApi,
    })
}

client.registry
    .registerDefaultTypes()
    .registerGroups([
        ['fun', 'Fun commands group'],
        ['misc', 'Miscellaneous commands group'],
        ['mod', 'Moderation commands group '],
        ['music', 'Music commands group '],
        ['nsfw', 'NSFW commands group'],
        ['reminders', 'Reminders/Time related commands group'],
        ['games', 'Commands related to games.'],
    ])
    .registerDefaultGroups()
    .registerTypesIn(path.join(__dirname, 'customTypes'))
    .registerDefaultCommands({
        help: false,
        prefix: false,
        unknownCommand: false,
    })
    .registerCommandsIn(path.join(__dirname, 'commands'))

client.on('message', message => {
    sendF(message)
    remindAggin(message)
})

client.on('ready', async () => {

    client.logger.info(`[READY] Logged in as ${client.user.tag}! ID: ${client.user.id}`)

    // Set up existing timers
    await client.timers.fetchAll()

    // Push client-related activities
    client.activities.push(
        { text: () => `${formatNumber(client.guilds.cache.size)} servers`, type: 'WATCHING' },
        { text: () => `with ${formatNumber(client.registry.commands.size)} commands`, type: 'PLAYING' },
        { text: () => `${formatNumber(client.channels.cache.size)} channels`, type: 'WATCHING' },
    )

    // Interval to change activity every minute
    client.setInterval(() => {
        const activity = client.activities[Math.floor(Math.random() * client.activities.length)]
        const text = typeof activity.text === 'function' ? activity.text() : activity.text
        client.user.setActivity(text, { type: activity.type })
    }, 60000)

    // Start statcord auto posting
    if (!config.debug) await statcord.autopost()
})

client.on('disconnect', event => {
    client.logger.error(`[DISCONNECT] Disconnected with code ${event.code}.`)
    process.exit(0)
})

client.on('error', error => client.logger.error(error.stack))

client.on('warn', warn => client.logger.warn(warn))

client.on('commandRun', (command, promise, message) => {
    if (command.uses === undefined) return
    command.uses++
    client.logger.info(chalk.cyan(`${message.author.id}/${message.author.username}#${message.author.discriminator}: ${message.content}`))
    if (!config.debug) statcord.postCommand(command.name, message.author.id)
})

client.on('commandError', (command, err) => client.logger.error(`[COMMAND:${command.name}]\n${err.stack}`))

if (!config.debug && statcord != null) {
    statcord.on('autopost-start', () => {
        client.logger.info('Started autopost')
    })

    statcord.on('post', status => {
        if (status) { client.logger.error(`Statcord: ${status}`) }
    })
}

client.login(config.token)
    .then(async () => {
        if(ayodhyaWeather == null) {
            await getAyodhyaWeatherData()
        }
        schedule.scheduleJob('0 */4 * * *', async function() {
            await getAyodhyaWeatherData().then(() => {
                sendJaiShreeRamGreeting()
            })
        })
    }).catch(error => {
        console.log(chalk.redBright(`Error while logging in. Message: ${error.message}`))
    })

function sendF(message) {
    if (message.content.toLowerCase() === 'f' && !message.author.bot) {
        return message.channel.send('f')
    }
}

function remindAggin(message) {
    if (message.content.toLowerCase().includes('nigga') && !message.author.bot) {
        return message.channel.send('Why do I have to remind you everytime?, It\'s aggiN')
    }
}

async function getAyodhyaWeatherData() {
    ayodhyaWeather = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=Ajodhya&appid=${config.openWeatherKey}`)
        .then(response => response.json())
}

async function sendJaiShreeRamGreeting() {
    if (ayodhyaWeather != null) {
        const morningDate = new Date(0).setUTCSeconds(
            ayodhyaWeather.sys.sunrise)
        const nightDate = new Date(0).setUTCSeconds(ayodhyaWeather.sys.sunset)
        schedule.scheduleJob(morningDate, function() {
            const channel = client.channels.cache.find(
                channelName => channelName.name === 'chit-chats')
            channel.send('Good morning. Jay Shree Ram!')
            console.log('Good morning. Jay Shree Ram!')
        })
        schedule.scheduleJob(nightDate, function() {
            const channel = client.channels.cache.find(
                channelName => channelName.name === 'chit-chats')
            channel.send('Good night. Jay Shree Ram!')
            console.log('Good night. Jay Shree Ram!')
        })
    }
}